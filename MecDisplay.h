/**
 * ****************************************************************************
 * 
 * * MecDisplay
 * 
 * Libary for Handeling an additional display
 * ****************************************************************************
 * By Michael Egtved Christensen 2020 ->
 * 
 * NOTE:
 * ? For usage see reference implementation
 * 
 * 
 * 
 * 
 *
 * 
*/
#ifndef MecDisplay_h
#define MecDisplay_h

#include <Arduino.h>
#include <MecLogging.h>
#include <MecWifi.h>
#include <MecWebThing.h>

#include "FS.h"
#include <SPI.h>
#include <TFT_eSPI.h>
#include <lvgl.h>

#define LVGL_TICK_PERIOD 60

// This is the file name used to store the calibration data
// You can change this to create new calibration files.
// The SPIFFS file name must start with "/".
#define CALIBRATION_FILE "/TouchCalData"

// Set REPEAT_CAL to true instead of false to run calibration
// again, otherwise it will only be done once.
// Repeat calibration if you change the screen rotation.
#define REPEAT_CAL false

class MecDisplay
{

public:
  MecDisplay(MecLogging(*MyLog), MecWifi(*MyWifi), MecThing(*MyThing));
  void setup();
  void loop();

  //void my_disp_flush(lv_disp_drv_t *disp, const lv_area_t *area, lv_color_t *color_p);

  void drawSetup();
  void saveSetup();
  void drawLog();
  void drawMain();
  void saveMain();

  MecLogging *_MyLog;

private:
  void touch_calibrate();

  MecWifi *_MyWifi;
  MecThing *_MyThing;
  long _update = 0;

  void createSetup();
  void createLog();
  void createMain();

  //lv_obj_t *txtAPPassword;
  lv_obj_t *txtAPName;
  lv_obj_t *txtAPPassword;
  lv_obj_t *chkEnableAP;
  lv_obj_t *txtWiFiSSID[10];
  lv_obj_t *txtWiFiPassword[10];
  lv_obj_t *SaveButton;

  // Properties
  lv_obj_t *obj_properties[10];
};

#endif
