#include <MecDisplay.h>

#define WiFI_CONFIG_FILE "/WiFICONFIGFILE"

static lv_disp_buf_t disp_buf;
static lv_color_t buf[LV_HOR_RES_MAX * 10];

TFT_eSPI TFTDisplay = TFT_eSPI(); // Invoke custom library

lv_obj_t *tabview;
lv_obj_t *tabMain;
lv_obj_t *tabLog;
lv_obj_t *tabSetup;

static lv_style_t style_box;
lv_obj_t *kb; // keyboard

int screenWidth = 480;
int screenHeight = 320;

MecDisplay *__MyMecDisplay;
lv_obj_t *infocus; // obj id på det objekt der bliver ændret.

MecDisplay::MecDisplay(MecLogging(*MyLog), MecWifi(*MyWifi), MecThing(*MyThing))
{

    _MyLog = MyLog;
    _MyWifi = MyWifi;
    _MyThing = MyThing;
    (*_MyLog).infoNl("MecThing: Created", Lib);

    __MyMecDisplay = this;
};

/**
 * ****************************************************************************
 * ****************************************************************************
 * Display flushing og andre rutiner der kraeves af lvgl...
 * ****************************************************************************
*/
void my_disp_flush(lv_disp_drv_t *disp, const lv_area_t *area, lv_color_t *color_p)
{
    uint16_t c;
    TFTDisplay.startWrite();                                                                            /* Start new TFT transaction */
    TFTDisplay.setAddrWindow(area->x1, area->y1, (area->x2 - area->x1 + 1), (area->y2 - area->y1 + 1)); /* set the working window */
    for (int y = area->y1; y <= area->y2; y++)
    {
        for (int x = area->x1; x <= area->x2; x++)
        {
            c = color_p->full;
            TFTDisplay.writeColor(c, 1);
            color_p++;
        }
    }
    TFTDisplay.endWrite();     /* terminate TFT transaction */
    lv_disp_flush_ready(disp); /* tell lvgl that flushing is done */
}

bool my_touchpad_read(lv_indev_drv_t *indev_driver, lv_indev_data_t *data)
{
    uint16_t touchX, touchY;

    bool touched = TFTDisplay.getTouch(&touchX, &touchY, 600);

    if (!touched)
    {
        return false;
    }

    if (touchX > screenWidth || touchY > screenHeight)
    {
        Serial.println("Y or y outside of expected parameters..");
        Serial.print("y:");
        Serial.print(touchX);
        Serial.print(" x:");
        Serial.print(touchY);
    }
    else
    {
        data->state = touched ? LV_INDEV_STATE_PR : LV_INDEV_STATE_REL;
        /*Save the state and save the pressed coordinate*/
        //if(data->state == LV_INDEV_STATE_PR) touchpad_get_xy(&last_x, &last_y);
        /*Set the coordinates (if released use the last pressed coordinates)*/
        data->point.x = touchX;
        data->point.y = touchY;

        Serial.print("Data x");
        Serial.println(touchX);

        Serial.print("Data y");
        Serial.println(touchY);
    }

    return false; /*Return `false` because we are not buffering and no more data to read*/
}

void MecDisplay::touch_calibrate()
{
    uint16_t calData[5];
    uint8_t calDataOK = 0;

    // check if calibration file exists and size is correct
    if (SPIFFS.exists(CALIBRATION_FILE))
    {
        if (REPEAT_CAL)
        {
            // Delete if we want to re-calibrate
            (*_MyLog).infoNl("SPIFFS: Deleting Calibration file", War);
            SPIFFS.remove(CALIBRATION_FILE);
        }
        else
        {
            (*_MyLog).infoNl("SPIFFS: Reading Calibration file", Inf);
            File f = SPIFFS.open(CALIBRATION_FILE, "r");
            if (f)
            {
                if (f.readBytes((char *)calData, 14) == 14)
                    calDataOK = 1;
                f.close();
            }
        }
    }

    // https://github.com/Bodmer/TFT_eSPI/blob/master/examples/480%20x%20320/Keypad_480x320/Keypad_480x320.ino

    if (calDataOK && !REPEAT_CAL)
    {
        // calibration data valid
        TFTDisplay.setTouch(calData);
    }
    else
    {
        // data not valid so recalibrate
        TFTDisplay.fillScreen(TFT_BLACK);
        TFTDisplay.setCursor(20, 0);
        TFTDisplay.setTextFont(2);
        TFTDisplay.setTextSize(1);
        TFTDisplay.setTextColor(TFT_WHITE, TFT_BLACK);

        TFTDisplay.setTextFont(1);
        TFTDisplay.println("Touch corners as indicated");
        TFTDisplay.println();

        if (REPEAT_CAL)
        {
            TFTDisplay.setTextColor(TFT_RED, TFT_BLACK);
            TFTDisplay.println("Set REPEAT_CAL to false to stop this running again!");
        }

        TFTDisplay.calibrateTouch(calData, TFT_MAGENTA, TFT_BLACK, 15);

        TFTDisplay.setTextColor(TFT_GREEN, TFT_BLACK);
        TFTDisplay.println("Calibration complete!");
        (*_MyLog).infoNl("SPIFFS: Calibration complete!", Inf);

        // store data
        File f = SPIFFS.open(CALIBRATION_FILE, "w");
        if (f)
        {
            f.write((const unsigned char *)calData, 14);
            f.close();
        }
    }
}

/**
 * ****************************************************************************
 * ****************************************************************************
 * Callback rutiner til "Setup" 
 * ****************************************************************************
*/

static void event_setup_checkbox(lv_obj_t *_kb, lv_event_t e)
{
    if (e == LV_EVENT_VALUE_CHANGED)
    {
        (*__MyMecDisplay)._MyLog->infoNl("CHECKBOX, VALUE CHANGED", Inf);
        //(*__MyMecDisplay).saveSetup();
    }
}

static void event_setup_keyboard(lv_obj_t *_kb, lv_event_t e)
{
    lv_keyboard_def_event_cb(kb, e);

    if (e == LV_EVENT_CANCEL)
    {
        if (kb)
        {
            lv_obj_set_height(tabSetup, LV_VER_RES);
            lv_obj_del(kb);
            kb = NULL;
        }
        (*__MyMecDisplay)._MyLog->infoNl("TEKSTBOX, VALUE NOT CHANGED", Inf);
        infocus = NULL;
        (*__MyMecDisplay).drawSetup();
    }

    if (e == LV_EVENT_APPLY)
    {
        if (kb)
        {
            lv_obj_set_height(tabSetup, LV_VER_RES);
            lv_obj_del(kb);
            kb = NULL;
        }
        (*__MyMecDisplay)._MyLog->infoNl("TEKSTBOX, VALUE CHANGED", Inf);
        //(*__MyMecDisplay).saveSetup(); Nej ikke her, vi gemmer først når du trykker på knappen
        infocus = NULL;
    }
}

static void event_setup_tekstboks(lv_obj_t *ta, lv_event_t e)
{
    if (e == LV_EVENT_RELEASED)
    {
        if (kb == NULL)
        {
            lv_obj_set_height(tabSetup, LV_VER_RES / 2);
            kb = lv_keyboard_create(lv_scr_act(), NULL);

            lv_obj_set_event_cb(kb, event_setup_keyboard);

            lv_indev_wait_release(lv_indev_get_act());
        }
        lv_textarea_set_cursor_hidden(ta, false);
        //lv_page_focus(t1, lv_textarea_get_label(ta), LV_ANIM_ON);
        lv_keyboard_set_textarea(kb, ta);
        infocus = ta;
    }
    else if (e == LV_EVENT_DEFOCUSED)
    {
        lv_textarea_set_cursor_hidden(ta, true);
        infocus = NULL;
    }
}

static void event_setup_savebutton(lv_obj_t *ta, lv_event_t e)
{
    if (e == LV_BTN_STATE_PRESSED)
    {
        (*__MyMecDisplay)._MyLog->infoNl("Save and reboot button pressed", Inf);
        (*__MyMecDisplay).saveSetup();
    }
}

/**
 * ****************************************************************************
 * ****************************************************************************
 * Callback rutiner til "Main" 
 * ****************************************************************************
*/

static void event_main_number_keyboard(lv_obj_t *_kb, lv_event_t e)
{
    lv_keyboard_def_event_cb(kb, e);

    if (e == LV_EVENT_CANCEL)
    {
        if (kb)
        {
            lv_obj_set_height(tabSetup, LV_VER_RES);
            lv_obj_del(kb);
            kb = NULL;
        }
        (*__MyMecDisplay)._MyLog->infoNl("Main textbox, VALUE NOT CHANGED", Inf);
        infocus = NULL;
        (*__MyMecDisplay).drawMain();
    }

    if (e == LV_EVENT_APPLY)
    {
        if (kb)
        {
            lv_obj_set_height(tabSetup, LV_VER_RES);
            lv_obj_del(kb);
            kb = NULL;
        }
        (*__MyMecDisplay)._MyLog->infoNl("Main TEKSTBOX, VALUE CHANGED", Inf);
        (*__MyMecDisplay).saveMain();
        infocus = NULL;
    }
}

static void event_main_numberboks(lv_obj_t *ta, lv_event_t e)
{
    if (e == LV_EVENT_RELEASED)
    {
        if (kb == NULL)
        {
            lv_obj_set_height(tabSetup, LV_VER_RES / 2);
            kb = lv_keyboard_create(lv_scr_act(), NULL);
            lv_keyboard_set_mode(kb, LV_KEYBOARD_MODE_NUM);

            lv_obj_set_event_cb(kb, event_main_number_keyboard);

            lv_indev_wait_release(lv_indev_get_act());
        }
        lv_textarea_set_cursor_hidden(ta, false);
        //lv_page_focus(t1, lv_textarea_get_label(ta), LV_ANIM_ON);
        lv_keyboard_set_textarea(kb, ta);
        infocus = ta;
    }
    else if (e == LV_EVENT_DEFOCUSED)
    {
        lv_textarea_set_cursor_hidden(ta, true);
        infocus = NULL;
    }
}

static void event_main_text_keyboard(lv_obj_t *_kb, lv_event_t e)
{
    lv_keyboard_def_event_cb(kb, e);

    if (e == LV_EVENT_CANCEL)
    {
        if (kb)
        {
            lv_obj_set_height(tabSetup, LV_VER_RES);
            lv_obj_del(kb);
            kb = NULL;
        }
        (*__MyMecDisplay)._MyLog->infoNl("Main textbox, VALUE NOT CHANGED", Inf);
        infocus = NULL;
        (*__MyMecDisplay).drawMain();
    }

    if (e == LV_EVENT_APPLY)
    {
        if (kb)
        {
            lv_obj_set_height(tabSetup, LV_VER_RES);
            lv_obj_del(kb);
            kb = NULL;
        }
        (*__MyMecDisplay)._MyLog->infoNl("Main TEKSTBOX, VALUE CHANGED", Inf);
        (*__MyMecDisplay).saveMain();
        infocus = NULL;
    }
}

static void event_main_textboks(lv_obj_t *ta, lv_event_t e)
{
    if (e == LV_EVENT_RELEASED)
    {
        if (kb == NULL)
        {
            infocus = ta;
            lv_obj_set_height(tabSetup, LV_VER_RES / 2);
            kb = lv_keyboard_create(lv_scr_act(), NULL);

            lv_obj_set_event_cb(kb, event_main_text_keyboard);

            lv_indev_wait_release(lv_indev_get_act());
        }
        lv_textarea_set_cursor_hidden(ta, false);
        //lv_page_focus(t1, lv_textarea_get_label(ta), LV_ANIM_ON);
        lv_keyboard_set_textarea(kb, ta);
    }
    else if (e == LV_EVENT_DEFOCUSED)
    {
        lv_textarea_set_cursor_hidden(ta, true);
        infocus = NULL;
    }
}

static void event_main_checkbox(lv_obj_t *_kb, lv_event_t e)
{
    if (e == LV_EVENT_VALUE_CHANGED)
    {
        (*__MyMecDisplay)._MyLog->infoNl("CHECKBOX, VALUE CHANGED", Inf);
        infocus = _kb;
        (*__MyMecDisplay).saveMain();
        infocus = NULL;
    }
}
/**
 * ****************************************************************************
 * ****************************************************************************
 * Rutiner til Log 
 * ****************************************************************************
 * Create: bruges første gang og tegner GUI
 * Draw: Opdaterer data, aktiveres af loop, ca. hvert 3 sekunder.
*/

void MecDisplay::createLog()
{
    lv_obj_t *tabLogLabel = lv_label_create(tabLog, NULL);
    lv_label_set_text(tabLogLabel, ((*_MyWifi).LongWifiStatus() + (*_MyLog).GetLogArray()).c_str());
}

void MecDisplay::drawLog()
{
    lv_tabview_clean_tab(tabLog);
    lv_obj_t *tabLogLabel = lv_label_create(tabLog, NULL);
    lv_label_set_text(tabLogLabel, ("time: " + String(millis()) + "\n\n" + (*_MyWifi).LongWifiStatus() + (*_MyLog).GetLogArray()).c_str());
}

/**
 * ****************************************************************************
 * ****************************************************************************
 * Rutiner til Setup 
 * ****************************************************************************
 * Create: bruges første gang og tegner GUI
 * Draw: Opdaterer data, bruges kun hvis der trykkes "x", alså cancel på keyboardet
 * Save: Bruges når der trykkes på gemknappen
*/

void MecDisplay::createSetup()
{
    // et eller andet style noget
    lv_style_init(&style_box);
    lv_style_set_value_align(&style_box, LV_STATE_DEFAULT, LV_ALIGN_OUT_TOP_LEFT);
    lv_style_set_value_ofs_y(&style_box, LV_STATE_DEFAULT, -LV_DPX(5));

    //
    // Første blok - AP navn og Password
    //
    lv_obj_t *containerAcesspunkt = lv_cont_create(tabSetup, NULL);
    lv_obj_set_width(containerAcesspunkt, LV_HOR_RES - 25);
    lv_obj_set_height(containerAcesspunkt, 95);
    lv_obj_set_pos(containerAcesspunkt, 5, 35);
    lv_obj_add_style(containerAcesspunkt, LV_CONT_PART_MAIN, &style_box);
    lv_obj_set_drag_parent(containerAcesspunkt, true);
    lv_obj_set_style_local_value_str(containerAcesspunkt, LV_CONT_PART_MAIN, LV_STATE_DEFAULT, "Accespunkt");
    //
    // Create the AP name box
    //
    txtAPName = lv_textarea_create(containerAcesspunkt, NULL);
    lv_textarea_set_text(txtAPName, (*_MyWifi).mDNSName.c_str());
    lv_textarea_set_one_line(txtAPName, true);
    lv_textarea_set_cursor_hidden(txtAPName, true);
    lv_obj_set_width(txtAPName, LV_HOR_RES / 2 - 20);
    lv_obj_set_pos(txtAPName, 5, 25);
    lv_obj_set_event_cb(txtAPName, event_setup_tekstboks);

    lv_obj_t *lblAPName = lv_label_create(containerAcesspunkt, NULL);
    lv_label_set_text(lblAPName, "Navn:");
    lv_obj_align(lblAPName, txtAPName, LV_ALIGN_OUT_TOP_LEFT, 0, 0);
    //
    // Create the password box
    //
    txtAPPassword = lv_textarea_create(containerAcesspunkt, NULL);
    lv_textarea_set_text(txtAPPassword, (*_MyWifi).APPassword.c_str());
    //lv_textarea_set_pwd_mode(txtAPPassword, true);
    lv_textarea_set_one_line(txtAPPassword, true);
    lv_textarea_set_cursor_hidden(txtAPPassword, true);
    lv_obj_set_width(txtAPPassword, LV_HOR_RES / 2 - 30);
    lv_obj_set_pos(txtAPPassword, LV_HOR_RES / 2 - 5, 25);
    lv_obj_set_event_cb(txtAPPassword, event_setup_tekstboks);
    lv_obj_t *lblAPPassword = lv_label_create(containerAcesspunkt, NULL);
    lv_label_set_text(lblAPPassword, "Password:");
    lv_obj_align(lblAPPassword, txtAPPassword, LV_ALIGN_OUT_TOP_LEFT, 0, 0);

    // Checkbox
    chkEnableAP = lv_checkbox_create(containerAcesspunkt, NULL);
    lv_checkbox_set_static_text(chkEnableAP, "Skal accespunktet vaere taent");
    lv_checkbox_set_checked(chkEnableAP, (*_MyWifi).RunSoftAP);
    lv_obj_set_pos(chkEnableAP, 5, 65);
    lv_obj_set_event_cb(chkEnableAP, event_setup_checkbox);

    //
    // Anden blok WiFI forbindelser brugernavn og password
    //
    lv_obj_t *containerWifi = lv_cont_create(tabSetup, NULL);
    lv_obj_set_width(containerWifi, LV_HOR_RES - 25);
    lv_obj_set_height(containerWifi, (*_MyWifi).WifiCount * 40 + 65);
    lv_obj_set_pos(containerWifi, 5, 165);
    lv_obj_add_style(containerWifi, LV_CONT_PART_MAIN, &style_box);
    lv_obj_set_drag_parent(containerWifi, true);
    lv_obj_set_style_local_value_str(containerWifi, LV_CONT_PART_MAIN, LV_STATE_DEFAULT, "Wifi");

    for (int n = 0; n < (*_MyWifi).WifiCount; n++)
    {
        // Create the password box
        txtWiFiSSID[n] = lv_textarea_create(containerWifi, NULL);
        lv_textarea_set_text(txtWiFiSSID[n], (*_MyWifi).WiFiSSID[n].c_str());
        lv_textarea_set_one_line(txtWiFiSSID[n], true);
        lv_textarea_set_cursor_hidden(txtWiFiSSID[n], true);
        lv_obj_set_width(txtWiFiSSID[n], LV_HOR_RES / 2 - 20);
        lv_obj_set_pos(txtWiFiSSID[n], 5, 25 + 40 * n);
        lv_obj_set_event_cb(txtWiFiSSID[n], event_setup_tekstboks);

        txtWiFiPassword[n] = lv_textarea_create(containerWifi, NULL);
        lv_textarea_set_text(txtWiFiPassword[n], (*_MyWifi).WiFiPassword[n].c_str());
        //lv_textarea_set_text(txtWiFiPassword[n], "xxxxxxxx");
        lv_textarea_set_pwd_mode(txtWiFiPassword[n], true);
        lv_textarea_set_one_line(txtWiFiPassword[n], true);
        lv_textarea_set_cursor_hidden(txtWiFiPassword[n], true);
        lv_obj_set_width(txtWiFiPassword[n], LV_HOR_RES / 2 - 30);
        lv_obj_set_pos(txtWiFiPassword[n], LV_HOR_RES / 2 - 5, 25 + 40 * n);
        lv_obj_set_event_cb(txtWiFiPassword[n], event_setup_tekstboks);
        if (n == 0)
        {
            lv_obj_t *lblmDNSName = lv_label_create(containerWifi, NULL);
            lv_label_set_text(lblmDNSName, "Navn:");
            lv_obj_align(lblmDNSName, txtWiFiSSID[n], LV_ALIGN_OUT_TOP_LEFT, 0, 0);

            lv_obj_t *lblWiFiPassword = lv_label_create(containerWifi, NULL);
            lv_label_set_text(lblWiFiPassword, "Password:");
            lv_obj_align(lblWiFiPassword, txtWiFiPassword[n], LV_ALIGN_OUT_TOP_LEFT, 0, 0);
        }
    }

    txtWiFiSSID[(*_MyWifi).WifiCount] = lv_textarea_create(containerWifi, NULL);
    lv_textarea_set_text(txtWiFiSSID[(*_MyWifi).WifiCount], "");
    lv_textarea_set_placeholder_text(txtWiFiSSID[(*_MyWifi).WifiCount], "Ny WiFi?");
    lv_textarea_set_one_line(txtWiFiSSID[(*_MyWifi).WifiCount], true);
    lv_textarea_set_cursor_hidden(txtWiFiSSID[(*_MyWifi).WifiCount], true);
    lv_obj_set_width(txtWiFiSSID[(*_MyWifi).WifiCount], LV_HOR_RES / 2 - 20);
    lv_obj_set_pos(txtWiFiSSID[(*_MyWifi).WifiCount], 5, 25 + 40 * (*_MyWifi).WifiCount);
    lv_obj_set_event_cb(txtWiFiSSID[(*_MyWifi).WifiCount], event_setup_tekstboks);

    txtWiFiPassword[(*_MyWifi).WifiCount] = lv_textarea_create(containerWifi, NULL);
    lv_textarea_set_text(txtWiFiPassword[(*_MyWifi).WifiCount], "");
    lv_textarea_set_placeholder_text(txtWiFiPassword[(*_MyWifi).WifiCount], "Password?");
    //lv_textarea_set_pwd_mode(txtAPPassword, true);
    lv_textarea_set_one_line(txtWiFiPassword[(*_MyWifi).WifiCount], true);
    lv_textarea_set_cursor_hidden(txtWiFiPassword[(*_MyWifi).WifiCount], true);
    lv_obj_set_width(txtWiFiPassword[(*_MyWifi).WifiCount], LV_HOR_RES / 2 - 30);
    lv_obj_set_pos(txtWiFiPassword[(*_MyWifi).WifiCount], LV_HOR_RES / 2 - 5, 25 + 40 * (*_MyWifi).WifiCount);
    lv_obj_set_event_cb(txtWiFiPassword[(*_MyWifi).WifiCount], event_setup_tekstboks);

    // Knapper
    lv_obj_t *containerButtons = lv_cont_create(tabSetup, NULL);
    lv_obj_set_width(containerButtons, LV_HOR_RES - 10);
    lv_obj_set_height(containerButtons, 120);
    lv_obj_set_pos(containerButtons, 5, 235 + (*_MyWifi).WifiCount * 40);
    lv_obj_add_style(containerButtons, LV_CONT_PART_MAIN, &style_box);
    lv_obj_set_drag_parent(containerButtons, true);
    //    lv_obj_set_style_local_value_str(containerButtons, LV_CONT_PART_MAIN, LV_STATE_DEFAULT, "Actions");

    SaveButton = lv_btn_create(containerButtons, NULL);
    lv_obj_t *label = lv_label_create(SaveButton, NULL);

    lv_label_set_text(label, " Save and reboot ");
    lv_btn_set_checkable(SaveButton, true);
    lv_obj_set_event_cb(SaveButton, event_setup_savebutton);
    //   lv_obj_set_pos(SaveButton, 5 + LV_HOR_RES / 2, 235 + (*_MyWifi).WifiCount * 40);
    lv_obj_set_pos(SaveButton, 5 + LV_HOR_RES / 2, 15);
}

void MecDisplay::drawSetup()
{
    if (infocus != txtAPName)
    {
        lv_textarea_set_text(txtAPName, (*_MyWifi).mDNSName.c_str());
    }
    if (infocus != txtAPPassword)
    {
        lv_textarea_set_text(txtAPPassword, (*_MyWifi).APPassword.c_str());
    }
    if (infocus != chkEnableAP)
    {
        lv_checkbox_set_checked(chkEnableAP, (*_MyWifi).RunSoftAP);
    }
    for (int i = 0; i < 10; i++)
    {
        if (txtWiFiPassword[i] != NULL)
        {
            if (infocus != txtWiFiPassword[i])
            {
                lv_textarea_set_text(txtWiFiPassword[i], (*_MyWifi).WiFiPassword[i].c_str());
            }
            if (infocus != txtWiFiSSID[i])
            {
                lv_textarea_set_text(txtWiFiSSID[i], (*_MyWifi).WiFiSSID[i].c_str());
            }
        }
    }
};

void MecDisplay::saveSetup()
{
    if (SPIFFS.exists(WiFI_CONFIG_FILE))
    {
        // Delete if we want to re-calibrate
        (*_MyLog).infoNl("Thing:SPIFFS: Deleting WiFI_CONFIG_FILE file", Inf);
        SPIFFS.remove(WiFI_CONFIG_FILE);
    }

    // store data
    File f = SPIFFS.open(WiFI_CONFIG_FILE, "w");
    if (f)
    {

        (*_MyLog).infoNl("Get: mDNSName result: " + String(lv_textarea_get_text(txtAPName)), Inf);
        f.print(String(lv_textarea_get_text(txtAPName)) + '\n');
        //    f.write(server.arg("mDNSName"));
        (*_MyLog).infoNl("Get: APPassword result: " + String(lv_textarea_get_text(txtAPPassword)), Inf);
        f.print(String(lv_textarea_get_text(txtAPPassword)) + '\n');

        (*_MyLog).infoNl("Get: RunSoftAP result: ", Inf);

        if (lv_checkbox_get_state(chkEnableAP) == LV_BTN_STATE_CHECKED_RELEASED ||
            lv_checkbox_get_state(chkEnableAP) == LV_BTN_STATE_CHECKED_PRESSED ||
            lv_checkbox_get_state(chkEnableAP) == LV_BTN_STATE_CHECKED_DISABLED)
        {
            f.print("true\n");
            (*_MyLog).infoNl("Get: FALSE", Inf);
        }
        else
        {
            f.print("false\n");
            (*_MyLog).infoNl("Get: FALSE", Inf);
        }

        for (int i = 0; i < 10; i++)
        {
            if (txtWiFiPassword[i] != NULL)
            {
                (*_MyLog).infoNl("Get: WiFiSSID" + String(i) + " result: " + lv_textarea_get_text(txtWiFiSSID[i]), Inf);
                f.print(String(lv_textarea_get_text(txtWiFiSSID[i])) + "\n");
                (*_MyLog).infoNl("Get: WiFiPassword" + String(i) + " result: " + lv_textarea_get_text(txtWiFiPassword[i]), Inf);
                f.print(String(lv_textarea_get_text(txtWiFiPassword[i])) + "\n");
            }
        }
    }
    f.close();

    f = SPIFFS.open(WiFI_CONFIG_FILE, "r");
    char buffer[64];

    while (f.available())
    {
        int l = f.readBytesUntil('\n', buffer, sizeof(buffer));
        buffer[l] = 0;
        (*_MyLog).infoNl(buffer, Inf);
    }
    //if (f.readBytes((char *)calData, 14) == 14)
    //  calDataOK = 1;
    f.close();

    // Virker kun på ESP32
    ESP.restart();
};

/**
 * ****************************************************************************
 * ****************************************************************************
 * Rutiner til Main 
 * ****************************************************************************
 * Create: bruges første gang og tegner GUI
 * Draw: Opdaterer data, bruges kun hvis der trykkes "x", alså cancel på keyboardet
 * Save: Bruges når der trykkes på gemknappen
*/

void MecDisplay::createMain()
{
    // et eller andet style noget
    lv_style_init(&style_box);
    lv_style_set_value_align(&style_box, LV_STATE_DEFAULT, LV_ALIGN_OUT_TOP_LEFT);
    lv_style_set_value_ofs_y(&style_box, LV_STATE_DEFAULT, -LV_DPX(5));
    // lv_style_set_margin_top(&style_box, LV_STATE_DEFAULT, LV_DPX(10));

    //
    // Første blok -
    //
    lv_obj_t *containerPropterties = lv_cont_create(tabMain, NULL);
    lv_obj_set_width(containerPropterties, LV_HOR_RES - 25);
    lv_obj_set_height(containerPropterties, (*_MyThing).CountOfPropterties * 40 + 25);
    lv_obj_set_pos(containerPropterties, 5, 35);
    lv_obj_add_style(containerPropterties, LV_CONT_PART_MAIN, &style_box);
    lv_obj_set_drag_parent(containerPropterties, true);
    lv_obj_set_style_local_value_str(containerPropterties, LV_CONT_PART_MAIN, LV_STATE_DEFAULT, (*_MyThing).Title.c_str());

    (_MyLog)->infoNl("Tegn MAIN", Inf);

    for (int i = 0; i < (*_MyThing).CountOfPropterties; i++)
    {
        (_MyLog)->infoNl("TegnMain Loop " + (*_MyThing).MecProperties[i]->ID + (*_MyThing).MecProperties[i]->Type, Inf);

        if ((*_MyThing).MecProperties[i]->Type == TypeString)
        {
            (_MyLog)->infoNl("TextThing", Inf);
            // Tekstinput
            obj_properties[i] = lv_textarea_create(containerPropterties, NULL);
            lv_textarea_set_text(obj_properties[i], (*_MyThing).MecProperties[i]->GetStringValue().c_str());
            lv_textarea_set_one_line(obj_properties[i], true);
            lv_textarea_set_cursor_hidden(obj_properties[i], true);
            lv_obj_set_width(obj_properties[i], LV_HOR_RES / 2 - 35);
            if ((*_MyThing).MecProperties[i]->ReadOnly == false)
            {
                lv_obj_set_event_cb(obj_properties[i], event_main_textboks);
            }
            lv_obj_set_pos(obj_properties[i], 5 + LV_HOR_RES / 2, 10 + 40 * i);

            lv_obj_t *tmp_lbl = lv_label_create(containerPropterties, NULL);
            lv_label_set_text(tmp_lbl, (*_MyThing).MecProperties[i]->Title.c_str());
            lv_obj_set_pos(tmp_lbl, 15, 20 + 40 * i);
        }
        if ((*_MyThing).MecProperties[i]->Type == TypeBoolean)
        {
            (_MyLog)->infoNl(" tegner Checkbox", Inf);
            // Checkbox
            obj_properties[i] = lv_checkbox_create(containerPropterties, NULL);
            lv_checkbox_set_static_text(obj_properties[i], (*_MyThing).MecProperties[i]->Title.c_str());
            lv_checkbox_set_checked(obj_properties[i], (*_MyThing).MecProperties[i]->GetBooleanValue());

            if ((*_MyThing).MecProperties[i]->ReadOnly)
            {
                lv_checkbox_set_disabled(obj_properties[i]);
            }
            else
            {
                lv_obj_set_event_cb(obj_properties[i], event_main_checkbox);
            }
            lv_obj_set_pos(obj_properties[i], 15, 10 + 40 * i);
        }
        if ((*_MyThing).MecProperties[i]->Type == TypeNumber)
        {
            // Decimaltal
            (_MyLog)->infoNl("Decimalting", Inf);

            obj_properties[i] = lv_textarea_create(containerPropterties, NULL);
            lv_textarea_set_text(obj_properties[i], (*_MyThing).MecProperties[i]->GetStringValue().c_str());
            lv_textarea_set_one_line(obj_properties[i], true);
            lv_textarea_set_cursor_hidden(obj_properties[i], true);
            lv_obj_set_width(obj_properties[i], LV_HOR_RES / 2 - 35);
            if ((*_MyThing).MecProperties[i]->ReadOnly == false)
            {
                lv_obj_set_event_cb(obj_properties[i], event_main_numberboks);
            }
            lv_textarea_set_accepted_chars(obj_properties[i], "0123456789+-.");

            lv_obj_set_pos(obj_properties[i], 5 + LV_HOR_RES / 2, 10 + 40 * i);

            lv_obj_t *tmp_lbl = lv_label_create(containerPropterties, NULL);
            lv_label_set_text(tmp_lbl, (*_MyThing).MecProperties[i]->Title.c_str());
            lv_obj_set_pos(tmp_lbl, 15, 20 + 40 * i);
        }

        if ((*_MyThing).MecProperties[i]->Type == TypeInteger)
        {
            // Heltal
            (_MyLog)->infoNl("Heltal", Inf);
            obj_properties[i] = lv_textarea_create(containerPropterties, NULL);
            lv_textarea_set_text(obj_properties[i], (*_MyThing).MecProperties[i]->GetStringValue().c_str());
            lv_textarea_set_one_line(obj_properties[i], true);
            lv_textarea_set_cursor_hidden(obj_properties[i], true);
            lv_obj_set_width(obj_properties[i], LV_HOR_RES / 2 - 35);
            {
                lv_obj_set_event_cb(obj_properties[i], event_main_numberboks);
            }
            lv_textarea_set_accepted_chars(obj_properties[i], "0123456789+-");

            lv_obj_set_pos(obj_properties[i], 5 + LV_HOR_RES / 2, 10 + 40 * i);

            lv_obj_t *tmp_lbl = lv_label_create(containerPropterties, NULL);
            lv_label_set_text(tmp_lbl, (*_MyThing).MecProperties[i]->Title.c_str());
            lv_obj_set_pos(tmp_lbl, 15, 20 + 40 * i);
        }
    }
}

void MecDisplay::drawMain()
{
    //(_MyLog)->infoNl("DrawMain", Inf);
    for (int i = 0; i < (*_MyThing).CountOfPropterties; i++)
    {
        if (infocus != obj_properties[i])
        {
            if ((*_MyThing).MecProperties[i]->Type == TypeString)
            {
                lv_textarea_set_text(obj_properties[i], (*_MyThing).MecProperties[i]->GetStringValue().c_str());
            }
            if ((*_MyThing).MecProperties[i]->Type == TypeBoolean)
            {
                lv_checkbox_set_checked(obj_properties[i], (*_MyThing).MecProperties[i]->GetBooleanValue());
            }
            if ((*_MyThing).MecProperties[i]->Type == TypeNumber)
            {
                lv_textarea_set_text(obj_properties[i], (*_MyThing).MecProperties[i]->GetStringValue().c_str());
            }

            if ((*_MyThing).MecProperties[i]->Type == TypeInteger)
            {
                lv_textarea_set_text(obj_properties[i], (*_MyThing).MecProperties[i]->GetStringValue().c_str());
            }
        }
    }
}

void MecDisplay::saveMain()
{
    (_MyLog)->infoNl("DrawSave", Inf);
    for (int i = 0; i < (*_MyThing).CountOfPropterties; i++)
    {
        if (infocus == obj_properties[i])
        {
            if ((*_MyThing).MecProperties[i]->Type == TypeString)
            {
                (*_MyThing).MecProperties[i]->SetValue(String(lv_textarea_get_text(obj_properties[i])), _ChangedFromRemote);
            }
            if ((*_MyThing).MecProperties[i]->Type == TypeBoolean)
            {
                if (lv_checkbox_get_state(chkEnableAP) == LV_BTN_STATE_CHECKED_RELEASED ||
                    lv_checkbox_get_state(chkEnableAP) == LV_BTN_STATE_CHECKED_PRESSED ||
                    lv_checkbox_get_state(chkEnableAP) == LV_BTN_STATE_CHECKED_DISABLED)
                {
                    (*_MyThing).MecProperties[i]->SetValue(true, _ChangedFromRemote);
                }
                else
                {
                    (*_MyThing).MecProperties[i]->SetValue(false, _ChangedFromRemote);
                }
            }
            if ((*_MyThing).MecProperties[i]->Type == TypeNumber)
            {
                (*_MyThing).MecProperties[i]->SetValue((float)String(lv_textarea_get_text(obj_properties[i])).toFloat(), _ChangedFromRemote);
            }

            if ((*_MyThing).MecProperties[i]->Type == TypeInteger)
            {
                (*_MyThing).MecProperties[i]->SetValue((int)String(lv_textarea_get_text(obj_properties[i])).toInt(), _ChangedFromRemote);
            }
        }
    }
}

/**
 * ****************************************************************************
 * ****************************************************************************
 * Rutiner til setup og loop 
 * ****************************************************************************
*/

void MecDisplay::setup()
{
    // Display
    lv_init();
    TFTDisplay.init();
    TFTDisplay.setRotation(3);
    // Calibrate the touch screen and retrieve the scaling factors
    touch_calibrate();
    // Clear the screen
    TFTDisplay.fillScreen(TFT_BLACK);

    /*Initialize the display*/
    lv_disp_buf_init(&disp_buf, buf, NULL, LV_HOR_RES_MAX * 10);
    lv_disp_drv_t disp_drv;
    lv_disp_drv_init(&disp_drv);
    disp_drv.hor_res = screenWidth;
    disp_drv.ver_res = screenHeight;
    disp_drv.flush_cb = my_disp_flush;
    disp_drv.buffer = &disp_buf;
    lv_disp_drv_register(&disp_drv);

    /*Initialize the input device driver*/
    lv_indev_drv_t indev_drv;
    lv_indev_drv_init(&indev_drv);          /*Descriptor of a input device driver*/
    indev_drv.type = LV_INDEV_TYPE_POINTER; /*Touch pad is a pointer-like device*/
    indev_drv.read_cb = my_touchpad_read;   /*Set your driver function*/
    lv_indev_drv_register(&indev_drv);      /*Finally register the driver*/

    /*Create a Tab view object*/
    tabview = lv_tabview_create(lv_scr_act(), NULL);

    /*Add 3 tabs (the tabs are page (lv_page) and can be scrolled*/
    tabMain = lv_tabview_add_tab(tabview, "Main");
    tabLog = lv_tabview_add_tab(tabview, "Log");
    tabSetup = lv_tabview_add_tab(tabview, "Setup");

    /*Add content to the tabs*/
    createLog();
    createMain();
    createSetup();
}

void MecDisplay::loop()
{

    lv_task_handler(); /* let the GUI do its work */

    if (_update < millis())
    {
        drawLog();
        drawMain();
        _update = millis() + 2000;
    }
}